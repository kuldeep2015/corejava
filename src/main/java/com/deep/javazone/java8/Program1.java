package com.deep.javazone.java8;

/**
 * The Class Program1.
 */
public class Program1 {
	
	public TempVariable tempVariable = (int value1, int value2)->{
		int sum = value1 + value2;
		System.out.println("Sum of two numbers:- " + sum);
	};

}

interface TempVariable{
	
	public void apply(int value1, int value2);
	
}
