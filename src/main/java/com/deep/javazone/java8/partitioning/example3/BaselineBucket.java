package com.deep.javazone.java8.partitioning.example3;

public class BaselineBucket {

	private String bucketCode;
	private String bucketCategory;
	private int sequence;
	private String treeLevel;
	private String treeName;
	private String treeNode;
	
	public BaselineBucket(String bucketCode, String bucketCategory, int sequence, 
			String treeName, String treeLevel, String treeNode)
	{
		this.bucketCode = bucketCode;
		this.bucketCategory = bucketCategory;
		this.sequence = sequence;
		this.treeName = treeName;
		this.treeLevel = treeLevel;
		this.treeNode = treeName;
	}
	
	public String getBucketCode() {
		return bucketCode;
	}
	public void setBucketCode(String bucketCode) {
		this.bucketCode = bucketCode;
	}
	public String getBucketCategory() {
		return bucketCategory;
	}
	public void setBucketCategory(String bucketCategory) {
		this.bucketCategory = bucketCategory;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public String getTreeLevel() {
		return treeLevel;
	}
	public void setTreeLevel(String treeLevel) {
		this.treeLevel = treeLevel;
	}
	public String getTreeName() {
		return treeName;
	}
	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}
	public String getTreeNode() {
		return treeNode;
	}
	public void setTreeNode(String treeNode) {
		this.treeNode = treeNode;
	}
}
