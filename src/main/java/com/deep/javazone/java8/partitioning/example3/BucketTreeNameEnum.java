package com.deep.javazone.java8.partitioning.example3;

public enum BucketTreeNameEnum {
	
	GL_ACCOUNT_GROUP("GL_ACCOUNT_GROUP",1),
	PRODUCT_GROUP("PRODUCT_GROUP",2),
	CUSTOMER_OWNER("CUSTOMER_OWNER",3),
	CURRENCY("CURRENCY",4),
	RATE_INDEX("RATE_INDEX",5);
	
	private String treeName;
	private Integer sequence;
	private BucketTreeNameEnum(String treeName, Integer sequence)
	{
		this.treeName = treeName;
		this.sequence = sequence;
	}
	
	public String getTreeName()
	{
		return this.treeName;
	}
	
	public int getSequence()
	{
		return this.sequence;
	}
	
	public static Integer getSequenceNumberByType(String treeName)
	{
		Integer sequenceNum = new Integer(0);
		
		
		for (BucketTreeNameEnum bucketTreeNameEnum : BucketTreeNameEnum.values()) {
			if(bucketTreeNameEnum.treeName.equals(treeName))
			{
				return bucketTreeNameEnum.getSequence();
			}
		}
		return sequenceNum;
	}
	
}
