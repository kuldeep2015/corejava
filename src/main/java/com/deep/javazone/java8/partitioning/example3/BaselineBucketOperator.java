package com.deep.javazone.java8.partitioning.example3;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BaselineBucketOperator {

	private static List<BaselineBucket> baselineBuckets = new ArrayList<>();
	private static final String BASELINE_CATEGORY = "BaseLine";
	static {
		//1-AccountGroup, 2-ProductGroup, 3-CustomerOwner, 4-Currency, 5-RateIndex
		//String bucketCode, String bucketCategory, int sequence, String treeName, String treeLevel, String treeNode
		
		baselineBuckets.add(new BaselineBucket("SGBaseLine000005", BASELINE_CATEGORY, BucketTreeNameEnum.GL_ACCOUNT_GROUP.getSequence(),
				BucketTreeNameEnum.GL_ACCOUNT_GROUP.getTreeName(), "5", "Asset"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000005", BASELINE_CATEGORY, BucketTreeNameEnum.PRODUCT_GROUP.getSequence(),
				BucketTreeNameEnum.PRODUCT_GROUP.getTreeName(), "5", "Trade"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000005", BASELINE_CATEGORY, BucketTreeNameEnum.CUSTOMER_OWNER.getSequence(),
				BucketTreeNameEnum.CUSTOMER_OWNER.getTreeName(), "5", "CUST_OWNER1"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000005", BASELINE_CATEGORY, BucketTreeNameEnum.CURRENCY.getSequence(),
				BucketTreeNameEnum.CURRENCY.getTreeName(), "5", "SGD"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000005", BASELINE_CATEGORY, BucketTreeNameEnum.RATE_INDEX.getSequence(),
				BucketTreeNameEnum.RATE_INDEX.getTreeName(), "5", "OTHERS"));
		
		baselineBuckets.add(new BaselineBucket("SGBaseLine000002", BASELINE_CATEGORY, BucketTreeNameEnum.GL_ACCOUNT_GROUP.getSequence(),
				BucketTreeNameEnum.GL_ACCOUNT_GROUP.getTreeName(), "2", "Asset"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000002", BASELINE_CATEGORY, BucketTreeNameEnum.PRODUCT_GROUP.getSequence(),
				BucketTreeNameEnum.PRODUCT_GROUP.getTreeName(), "2", "Trade"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000002", BASELINE_CATEGORY, BucketTreeNameEnum.CUSTOMER_OWNER.getSequence(),
				BucketTreeNameEnum.CUSTOMER_OWNER.getTreeName(), "2", "CUST_OWNER1"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000002", BASELINE_CATEGORY, BucketTreeNameEnum.CURRENCY.getSequence(),
				BucketTreeNameEnum.CURRENCY.getTreeName(), "2", "SGD"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000002", BASELINE_CATEGORY, BucketTreeNameEnum.RATE_INDEX.getSequence(),
				BucketTreeNameEnum.RATE_INDEX.getTreeName(), "2", "OTHERS"));
		
		baselineBuckets.add(new BaselineBucket("SGBaseLine000001", BASELINE_CATEGORY, BucketTreeNameEnum.GL_ACCOUNT_GROUP.getSequence(),
				BucketTreeNameEnum.GL_ACCOUNT_GROUP.getTreeName(), "1", "Asset"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000001", BASELINE_CATEGORY, BucketTreeNameEnum.PRODUCT_GROUP.getSequence(),
				BucketTreeNameEnum.PRODUCT_GROUP.getTreeName(), "1", "Trade"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000001", BASELINE_CATEGORY, BucketTreeNameEnum.CUSTOMER_OWNER.getSequence(),
				BucketTreeNameEnum.CUSTOMER_OWNER.getTreeName(), "1", "CUST_OWNER1"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000001", BASELINE_CATEGORY, BucketTreeNameEnum.CURRENCY.getSequence(),
				BucketTreeNameEnum.CURRENCY.getTreeName(), "1", "SGD"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000001", BASELINE_CATEGORY, BucketTreeNameEnum.RATE_INDEX.getSequence(),
				BucketTreeNameEnum.RATE_INDEX.getTreeName(), "1", "OTHERS"));
		
		
		
		
		
		baselineBuckets.add(new BaselineBucket("SGBaseLine000003", BASELINE_CATEGORY, BucketTreeNameEnum.GL_ACCOUNT_GROUP.getSequence(),
				BucketTreeNameEnum.GL_ACCOUNT_GROUP.getTreeName(), "3", "Asset"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000003", BASELINE_CATEGORY, BucketTreeNameEnum.PRODUCT_GROUP.getSequence(),
				BucketTreeNameEnum.PRODUCT_GROUP.getTreeName(), "3", "Trade"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000003", BASELINE_CATEGORY, BucketTreeNameEnum.CUSTOMER_OWNER.getSequence(),
				BucketTreeNameEnum.CUSTOMER_OWNER.getTreeName(), "3", "CUST_OWNER1"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000003", BASELINE_CATEGORY, BucketTreeNameEnum.CURRENCY.getSequence(),
				BucketTreeNameEnum.CURRENCY.getTreeName(), "3", "SGD"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000003", BASELINE_CATEGORY, BucketTreeNameEnum.RATE_INDEX.getSequence(),
				BucketTreeNameEnum.RATE_INDEX.getTreeName(), "3", "OTHERS"));
		
		
		baselineBuckets.add(new BaselineBucket("SGBaseLine000004", BASELINE_CATEGORY, BucketTreeNameEnum.GL_ACCOUNT_GROUP.getSequence(),
				BucketTreeNameEnum.GL_ACCOUNT_GROUP.getTreeName(), "4", "Asset"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000004", BASELINE_CATEGORY, BucketTreeNameEnum.PRODUCT_GROUP.getSequence(),
				BucketTreeNameEnum.PRODUCT_GROUP.getTreeName(), "4", "Trade"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000004", BASELINE_CATEGORY, BucketTreeNameEnum.CUSTOMER_OWNER.getSequence(),
				BucketTreeNameEnum.CUSTOMER_OWNER.getTreeName(), "4", "CUST_OWNER1"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000004", BASELINE_CATEGORY, BucketTreeNameEnum.CURRENCY.getSequence(),
				BucketTreeNameEnum.CURRENCY.getTreeName(), "4", "SGD"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000004", BASELINE_CATEGORY, BucketTreeNameEnum.RATE_INDEX.getSequence(),
				BucketTreeNameEnum.RATE_INDEX.getTreeName(), "4", "OTHERS"));
		

		baselineBuckets.add(new BaselineBucket("SGBaseLine000006", BASELINE_CATEGORY, BucketTreeNameEnum.GL_ACCOUNT_GROUP.getSequence(),
				BucketTreeNameEnum.GL_ACCOUNT_GROUP.getTreeName(), "6", "Asset"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000006", BASELINE_CATEGORY, BucketTreeNameEnum.PRODUCT_GROUP.getSequence(),
				BucketTreeNameEnum.PRODUCT_GROUP.getTreeName(), "6", "Trade"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000006", BASELINE_CATEGORY, BucketTreeNameEnum.CUSTOMER_OWNER.getSequence(),
				BucketTreeNameEnum.CUSTOMER_OWNER.getTreeName(), "6", "CUST_OWNER1"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000006", BASELINE_CATEGORY, BucketTreeNameEnum.CURRENCY.getSequence(),
				BucketTreeNameEnum.CURRENCY.getTreeName(), "6", "SGD"));
		baselineBuckets.add(new BaselineBucket("SGBaseLine000006", BASELINE_CATEGORY, BucketTreeNameEnum.RATE_INDEX.getSequence(),
				BucketTreeNameEnum.RATE_INDEX.getTreeName(), "6", "OTHERS"));

		
	}
	
	public static void main(String[] args)
	{	
		Map<String, List<BaselineBucket>> baselineBucketMap = 
				baselineBuckets.stream().collect(Collectors.groupingBy(BaselineBucket::getBucketCode,LinkedHashMap::new,
			Collectors.mapping(Function.identity(), Collectors.toList())
			));
				
				
				baselineBucketMap.forEach( (key, value)-> {System.out.println(key); 
				value.forEach(
						//System.out::println
						bucket-> System.out.println("[Sequence-" + bucket.getSequence() + "][TreeName" + bucket.getTreeName() + "]")
						);});
				System.out.println("---------------------------------------------------------------------------");
				
	}
}
