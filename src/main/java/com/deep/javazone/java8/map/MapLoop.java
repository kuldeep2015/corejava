package com.deep.javazone.java8.map;

import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class MapLoop.
 */
public class MapLoop {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
			
			Map<String, Integer> items = new HashMap<>();
			items.put("A", 10);
			items.put("B", 20);
			items.put("C", 30);
			items.put("D", 40);
			items.put("E", 50);
			items.put("F", 60);
			
			// loop prior java8
			normalForEachLoop(items);
			
			// Loop java8
			java8ForEachLoop(items);
			
		}
	
	/**
	 * Normal for each loop.
	 *
	 * @param items the items
	 */
	public static void normalForEachLoop(Map<String, Integer> items )
	{
		for (Map.Entry<String, Integer> entry : items.entrySet()) {
			System.out.println("Item : " + entry.getKey() + " Count : " + entry.getValue());
		}
	}
	
	/**
	 * Java 8 for each loop.
	 *
	 * @param items the items
	 */
	public static void java8ForEachLoop(Map<String, Integer> items )
	{
		items.forEach((k,v)->System.out.println("Item : " + k + " Count : " + v));
		
		items.forEach((k,v)->{
			System.out.println("Item : " + k + " Count : " + v);
			if("E".equals(k)){
				System.out.println("Hello E");
			}
		});
	}
}
