package com.deep.javazone.java8.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class ListLoop.
 */
public class ListLoop {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		List<String> items = new ArrayList<>();
		items.add("A");
		items.add("B");
		items.add("C");
		items.add("D");
		items.add("E");

		// loop prior java8
		normalForEachLoop(items);
		
		// Loop java8
		java8ForEachLoop(items);
		
	}
	
	/**
	 * Normal for each loop.
	 *
	 * @param items the items
	 */
	public static void normalForEachLoop(List<String> items)
	{
		for(String item : items){
			System.out.println(item);
		}
	}
	
	/**
	 * Java 8 for each loop.
	 *
	 * @param items the items
	 */
	public static void java8ForEachLoop(List<String> items)
	{
		//lambda
		//Output : A,B,C,D,E
		items.forEach(item->System.out.println(item));

		//Output : C
		items.forEach(item->{
			if("C".equals(item)){
				System.out.println(item);
			}
		});

		//method reference
		//Output : A,B,C,D,E
		items.forEach(System.out::println);

		//Stream and filter
		//Output : B
		items.stream()
			.filter(s->s.contains("B"))
			.forEach(System.out::println);
	}
}
