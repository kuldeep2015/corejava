package com.deep.javazone.java8.list.listtomap1;

// TODO: Auto-generated Javadoc
/**
 * The Class Hosting.
 */
public class Hosting {

    /** The Id. */
    private int Id;
    
    /** The name. */
    private String name;
    
    /** The websites. */
    private long websites;

    /**
     * Instantiates a new hosting.
     *
     * @param id the id
     * @param name the name
     * @param websites the websites
     */
    public Hosting(int id, String name, long websites) {
        Id = id;
        this.name = name;
        this.websites = websites;
    }

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return Id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		Id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the websites.
	 *
	 * @return the websites
	 */
	public long getWebsites() {
		return websites;
	}

	/**
	 * Sets the websites.
	 *
	 * @param websites the new websites
	 */
	public void setWebsites(long websites) {
		this.websites = websites;
	}

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
    	return "[Id-" + this.Id + "][name-" + this.name + "][website:" + this.websites + "]";
    }
    
    //getters, setters and toString()
}