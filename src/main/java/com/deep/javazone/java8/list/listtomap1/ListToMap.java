package com.deep.javazone.java8.list.listtomap1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// TODO: Auto-generated Javadoc
/**
 * The Class ListToMap.
 */
public class ListToMap {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		//listToMapCollectorsToMap();	
		listToMapDuplicatedKey();
	}
	
	/**
	 * List to map collectors to map.
	 */
	public static void listToMapCollectorsToMap()
	{
		 List<Hosting> list = new ArrayList<>();
	        list.add(new Hosting(1, "liquidweb.com", 80000));
	        list.add(new Hosting(2, "linode.com", 90000));
	        list.add(new Hosting(3, "digitalocean.com", 120000));
	        list.add(new Hosting(4, "aws.amazon.com", 200000));
	        list.add(new Hosting(5, "mkyong.com", 1));

	        // key = id, value - websites
	        Map<Integer, String> result1 = list.stream().collect(
	                Collectors.toMap(Hosting::getId, Hosting::getName));

	        System.out.println("Result 1 : " + result1);

	        // key = name, value - websites
	        Map<String, Long> result2 = list.stream().collect(
	                Collectors.toMap(Hosting::getName, Hosting::getWebsites));

	        System.out.println("Result 2 : " + result2);

	        // Same with result1, just different syntax
	        // key = id, value = name
	        Map<Integer, String> result3 = list.stream().collect(
	                Collectors.toMap(x -> x.getId(), x -> x.getName()));

	        System.out.println("Result 3 : " + result3);
	}
	
	/**
	 * List to map duplicated key.
	 */
	public static void listToMapDuplicatedKey()
	{
		List<Hosting> list = new ArrayList<>();
        list.add(new Hosting(1, "liquidweb.com", 80000));
        list.add(new Hosting(2, "linode.com", 90000));
        list.add(new Hosting(3, "digitalocean.com", 120000));
        list.add(new Hosting(4, "aws.amazon.com", 200000));
        list.add(new Hosting(5, "mkyong.com", 1));

        list.add(new Hosting(6, "linode.com", 100000)); // new line

        // key = name, value - websites , but the key 'linode' is duplicated!?
        Map<String, Long> result1 = list.stream().collect(
                Collectors.toMap(Hosting::getName, Hosting::getWebsites));

        System.out.println("Result 1 : " + result1);

    }
}

