package com.deep.javazone.java8.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ProgramLambda2 {
	
	public static void main(String[] args) {
		new ProgramLambda2().executeMethod();
	}
	
	
	private void executeMethod(){
		
		List<Integer> numberList = Arrays.asList(10, 20, 30);
		// Creating Lambda for Single number process
		Consumer<Integer> consumer = num-> System.out.println("Number is:- " + num);
		processNumber(numberList, consumer);
		
		// Creating Lambda for 2 numbers process
		BiConsumer<Integer, Integer> biConsumerDevision = (number, divisior) -> {
			System.out.println(number / divisior);
		};
		
		processNumberDevide(numberList, 0, biConsumerDevision);
		
	}
	
	private void processNumber(List<Integer> numList, Consumer<Integer> consumer){
		numList.forEach(number->{
			consumer.accept(number);
		});
	}
	
	private void processNumberDevide(List<Integer> numList, Integer divisor, BiConsumer<Integer, Integer> biConsumer){
		numList.forEach(number->{
			biConsumer.accept(number, divisor);
		});
	}
}
