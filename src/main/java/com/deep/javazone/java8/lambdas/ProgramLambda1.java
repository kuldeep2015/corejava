package com.deep.javazone.java8.lambdas;

public class ProgramLambda1 {
	
	public static void main(String[] args) {
		
		StringLengthLambda stringLengthLambda = (s)->{
			return s.length();
		};
		printLength(stringLengthLambda);
		
		//OR 
		StringLengthLambda stringLengthLambda1 = s -> s.length();
		printLength(stringLengthLambda1);
	}
	
	
	private static void printLength(StringLengthLambda stringLengthLambda){
		System.out.println(stringLengthLambda.stringLength("Hello World"));
	}
}

interface StringLengthLambda
{
	public int stringLength(String value);
}
