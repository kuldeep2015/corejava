package com.deep.javazone.java8.lambdas;

public class TestProgram {

	public static void main(String[] args) {
		
		GreetMessage greetMessage = ()->{
			System.out.println("In executeMethod");
		};
		
		greetMessage.showMessage();
	
		MathSum mathSum = (numA, numB)->{
			return numA+numB;
		};
		
		System.out.println( mathSum.doSum(5, 15) );
		
	}
}

interface GreetMessage{
	void showMessage();
}

interface MathSum{
	int doSum(int a, int b);
}
