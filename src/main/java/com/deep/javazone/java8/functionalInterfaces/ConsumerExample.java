package com.deep.javazone.java8.functionalInterfaces;

import java.util.function.Consumer;

public class ConsumerExample {

    public static void main(String[] args) {
        System.out.println("To Upper Case:- ");
        upperCaseFunction.accept("Hello World");

        System.out.println("To Lower Case:- ");
        lowerCaseFunction.accept("Hello World");

        System.out.println("Show length:- ");
        showLength.accept("Hello World");
    }


    // Functional interface for String uppercase
    private static Consumer<String> upperCaseFunction = (value)->{
        System.out.println(value.toUpperCase());
    };

    // Functional interface for String uppercase
    private static Consumer<String> lowerCaseFunction = (value)->{
        System.out.println(value.toLowerCase());
    };

    // Functional interface for calculating and showing length, without curley braces
    private static Consumer<String> showLength = (value)-> System.out.println(value.length());

}
