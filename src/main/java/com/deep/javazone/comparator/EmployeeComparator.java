package com.deep.javazone.comparator;

import com.deep.javazone.comparator.model.*;

import java.util.Comparator;

public class EmployeeComparator {

    public static Comparator<Employee> employeeNameComparator
            = Comparator.comparing(Employee::getName);

    public static Comparator<Employee> employeeAgeComparator
            = Comparator.comparing(Employee::getAge);

    public static Comparator<Employee> employeeNameThenAgeComparator
            = Comparator.comparing(Employee::getName).thenComparing(Employee::getAge);

    public static Comparator<Employee> employeeNumericPeriod
            = Comparator.comparing(Employee::getNumericPeriod);
}
