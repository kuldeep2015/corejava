package com.deep.javazone.comparator;

import com.deep.javazone.comparator.model.*;

import java.util.*;

public class EmployeeExecutor {

    public static void main(String[] args) {
        EmployeeExecutor employeeExecutor = new EmployeeExecutor();
        employeeExecutor.executeProcess();
    }

    private void executeProcess(){
        List<Employee> employees = getEmployeeList();
        System.out.println("Before sort: ");
        displayEmployee(employees);

        System.out.println("After sort by name:- ");
        Collections.sort(employees, EmployeeComparator.employeeNameComparator);
        displayEmployee(employees);

        System.out.println("After sort by Age:- ");
        Collections.sort(employees, EmployeeComparator.employeeAgeComparator);
        displayEmployee(employees);

        System.out.println("After sort by NameThenAge:- ");
        Collections.sort(employees, EmployeeComparator.employeeNameThenAgeComparator);
        displayEmployee(employees);

        System.out.println("After sort by NumericPeriod:- ");
        Collections.sort(employees, EmployeeComparator.employeeNumericPeriod);
        displayEmployee(employees);

    }

    private void displayEmployee(List<Employee> employees){
        employees.forEach(employee->{
            System.out.println("[Name:- " + employee.getName()
                    + ",[Age:- " + employee.getAge() + "]" + ",[Department:- " + employee.getDepartment() +"]"
                    + ",[NumericPeriod:- " + employee.getPeriod() +"]");
        });
    }

    private List<Employee> getEmployeeList(){
        List<Employee> employees = new ArrayList<>();

        Employee employee3 = new Employee();
        employee3.setName("Sanju");
        employee3.setAge(35);
        employee3.setDepartment("FINANCE");
        employee3.setPeriod("09");
        employees.add(employee3);

        Employee employee = new Employee();
        employee.setName("Peter");
        employee.setAge(30);
        employee.setDepartment("IT");
        employee.setPeriod("01");
        employees.add(employee);

        Employee employee2 = new Employee();
        employee2.setName("Ram");
        employee2.setAge(25);
        employee2.setDepartment("ACCOUNTS");
        employee2.setPeriod("12");
        employees.add(employee2);

        Employee employee4 = new Employee();
        employee4.setName("Zovelle");
        employee4.setAge(30);
        employee4.setDepartment("Infra");
        employee4.setPeriod("05");
        employees.add(employee4);

        Employee employee5 = new Employee();
        employee5.setName("Amkit");
        employee5.setAge(31);
        employee5.setDepartment("ITSS");
        employee5.setPeriod("02");
        employees.add(employee5);

        return employees;
    }
}
